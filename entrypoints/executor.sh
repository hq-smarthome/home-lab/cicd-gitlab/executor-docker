#!/usr/bin/env bash

set -ue

exitExecutor() {
echo "[executor] Terminating..."
  exit 0
}

waitForJob() {
  echo "[executor] Waiting for job to execute..."
  sleep 10m
  exit 124
}

trap exitExecutor EXIT

waitForJob
