#!/usr/bin/env bash

set -ue

CYAN='\033[0;36m'
NC='\033[0m'

echo -e "${CYAN}[pre-clone]${NC} Runner Script"

echo -e "${CYAN}[pre-clone] [Nomad]${NC} $(nomad version)"
echo -e "${CYAN}[pre-clone] [Terraform]${NC} $(terraform version)"
echo -e "${CYAN}[pre-clone] [Consul]${NC} $(consul version)"
echo -e "${CYAN}[pre-clone] [Vault]${NC} $(vault version)"
echo -e "${CYAN}[pre-clone] [Levant]${NC} $(levant version)"
