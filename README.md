# CICD GitLab Executor Docker Image

> :warning: **This project has been moved**
>
> Any further updates can be found here https://gitlab.com/carboncollins-cloud/cicd/gitlab-executor-container
> This repository will be archived in favour of all further development at the new location.

[[_TOC_]]

## Description

A (soon to be) multi-arch docker image to be used on all of the custom executor parametised jobs for building and deploying jobs within the hQ Home Lab.

## Basic Usage

These images are intended to be used by specific parametised jobs which are defined in seperate repositories. These can be found:

- [Linux AMD64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-amd64)
- ~~[Linux ARM](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-arm)~~ (Planned in the future)
- ~~[Linux ARM64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-arm64)~~ (Planned in the future)
- ~~[Darwin AMD64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-darwin-amd64)~~ (Planned in the future)

## Available Dependencies

The following dependencies are installed and are available for jobs to use within the executor:

- [git](https://git-scm.com/) - Version Control
- [GitLab Runner](https://docs.gitlab.com/runner/install/index.html) - GitLab CICD Agent
- [jq](https://stedolan.github.io/jq/) - Commandline JSON processor
- [Nomad](https://www.nomadproject.io/) - Workload Orchestration
- [Consul](https://www.consul.io/) - Serice Mesh
- [Vault](https://www.vaultproject.io/) - Secrets Management
- [Terraform](https://www.terraform.io/) - Infrastruture as Code
- [Levant](https://github.com/hashicorp/levant) - Nomad Job Templating

## Deployments

This job does not directly deploy to the cluster, however, this repo does trigger builds in all of the downstream executor repositories which in turn deploy the parametised jobs. See [Basic Usage](#basic-usage) for a list of downstream executors

## Authentication

Jobs running in the executor can make use of the `CI_JOB_JWT` variable to authenticate and obtain a vault token from the hQ cluster. This token will be bound against the repository itself limiting the scope of the permissions.

## Other Info

This repo has been moved to: https://gitlab.com/carboncollins-cloud/cicd/gitlab-executor-container. This existing repo will remain here archived as there are existing links to this repo.